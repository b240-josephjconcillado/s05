@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title mb-3">{{$post->title}}</h2>
            <p class="card-text text-muted">Author: {{$post->user->name}}</p>
            <p class="card-text text-muted">Likes: {{count($post->likes)}}</p>
            <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>


                {{-- Check if the user is logged in --}}
                @if(Auth::user())
                    {{-- Check if the post author is NOT the current user --}}
                    @if(Auth::user()->id != $post->user_id)
                        <form class="d-inline mr-1 " method="POST" action="/posts/{{$post->id}}/like">
                            @method('PUT')
                            @csrf
                            {{-- Check if the user has already liked the post --}}
                            @if($post->likes->contains("user_id", Auth::user()->id))
                                <button type="submit" class="btn btn-danger">Unlike</button>
                            @else
                                <button type="submit" class="btn btn-success">Like</button>
                            @endif
                        </form>
                        @endif
                        <!-- Button trigger modal -->
                         <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addComment">Add a comment</button>
             
                         <!-- Modal -->
                         <div class="modal fade" id="addComment" tabindex="-1" aria-labelledby="addCommentLabel" aria-hidden="true">
                             <div class="modal-dialog modal-dialog-centered">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h1 class="modal-title fs-5" id="addCommentLabel">Add a comment</h1>
                                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                     </div>
                                 <div class="modal-body">
                                     <form method="POST" action="/posts/{{$post->id}}/comment">
                                     @method('PUT')
                                     @csrf
                                         <div class="mb-3">
                                             <label for="recipient-name" class="col-form-label">User:</label>
                                             <input type="text" class="form-control" id="recipient-name" value="{{Auth::user()->name}}" disabled>
                                         </div>
                                         <div class="mb-3">
                                             <label for="message-text" class="col-form-label">Comment:</label>
                                             <textarea class="form-control" name="content" id="content"></textarea>
                                         </div>
                                         </div>
                                             <div class="modal-footer">
                                                 <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                 <button type="submit" class="btn btn-primary">Add</button>
                                             </div>
                                         </div>
                                     </form>
                             </div>
                         </div>
                @endif



            <div class="mt-3">
                <a href="/posts" class="card-link">View all posts</a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h2 class="card-title mb-2">Comments</h2>
            @if(count($post->comments)>0)
                @foreach($post->comments as $comment)
                    <div class="card mb-1">
                        <div class="card-body">
                            <h5 class="card-text mb-3">{{$comment->content}}</h5>
                            
                            <p class="card-subtitle mb-1 text-muted">{{$comment->user->name}}</p>
                            <p class="card-subtitle text-muted">Created at: {{$comment->created_at}}</p>
                        </div>
                    </div>
                @endforeach
            @else
                <div>
                    <h5>There are no comments</h5>
                </div>
            @endif
        </div>
    </div>
@endsection